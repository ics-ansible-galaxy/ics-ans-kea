import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_kea_package(host):
    p = host.package("kea")
    assert p.is_installed


def test_kea_service(host):
    s = host.service("kea-dhcp4")
    assert s.is_running
    assert s.is_enabled


def test_dhcp_port(host):
    # print(host.ansible.get_variables())
    # localip = host.ansible.get_variables()['ansible_default_ipv4']['address']
    assert host.socket("udp://%s:67" % host.interface("eth0").addresses[0]).is_listening
